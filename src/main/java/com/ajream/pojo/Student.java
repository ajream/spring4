package com.ajream.pojo;

import java.util.*;

public class Student {
    private String name;
    private Address address;
    private int age;
    private String[] books;
    private Map<String, Integer> grade;
    private Set<String> games;
    private List<String> hobbies;
    private Properties info;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getBooks() {
        return books;
    }

    public void setBooks(String[] books) {
        this.books = books;
    }

    public Map<String, Integer> getGrade() {
        return grade;
    }

    public void setGrade(Map<String, Integer> grade) {
        this.grade = grade;
    }

    public Set<String> getGames() {
        return games;
    }

    public void setGames(Set<String> games) {
        this.games = games;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public Properties getInfo() {
        return info;
    }

    public void setInfo(Properties info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Student{" +
                "\n name='" + name + '\'' +
                ", \n address=" + address +
                ", \n age=" + age +
                ", \n books=" + Arrays.toString(books) +
                ", \n grade=" + grade +
                ", \n games=" + games +
                ", \n hobbies=" + hobbies +
                ", \n info=" + info +
                "\n}";
    }
}
