



# spring容器注入

 [项目地址](https://codechina.csdn.net/m0_46079750/spring4/-/tree/master)



如果要注入的不是如`String` 这种简单类型数据，而是array、list、map、prop。。。这种容器类型数据，应该如何写bean：

[官方文档说明](https://docs.spring.io/spring-framework/docs/5.2.9.RELEASE/spring-framework-reference/core.html#beans-inner-beans)

```xml
<bean id="moreComplexObject" class="example.ComplexObject">
    <!-- array注入 -->
    <property name="books">
        <array>
            <value>《计算机技术》</value>
            <value>《操作系统》</value>
            <value>《数据结构》</value>
        </array>
    </property>
    
    <!-- prop注入 -->
    <property name="adminEmails">
        <props>
            <prop key="administrator">administrator@example.org</prop>
            <prop key="support">support@example.org</prop>
            <prop key="development">development@example.org</prop>
        </props>
    </property>
    
    <!-- list注入 -->
    <property name="someList">
        <list>
            <value>a list element followed by a reference</value>
            <ref bean="myDataSource" />
        </list>
    </property>
    
    <!-- map -->
    <property name="someMap">
        <map>
            <entry key="an entry" value="just some string"/>
            <entry key ="a ref" value-ref="myDataSource"/>
        </map>
    </property>
    
    <!-- set注入 -->
    <property name="someSet">
        <set>
            <value>just some string</value>
            <ref bean="myDataSource" />
        </set>
    </property>
</bean>
```



例如要往下面这些属性注入值

![image-20210730172726839](https://gitee.com/ajream/images/raw/master/img/2021-07-30 17-27-28_image-20210730172726839.png)

beans.xml：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
     https://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="address" class="com.ajream.pojo.Address" >
        <property name="addr" value="西安"/>
    </bean>

    <bean id="student" class="com.ajream.pojo.Student">
        
<!--        普通值注入-->
        <property name="name" value="王二"/>
        <property name="age" value="30"/>

<!--        bean注入-->
        <property name="address" ref="address" />

<!--        数组array注入-->
        <property name="books">
            <array>
                <value>《计算机技术》</value>
                <value>《操作系统》</value>
                <value>《数据结构》</value>
            </array>
        </property>

<!--        set注入-->
        <property name="games">
            <set>
                <value>王者荣耀</value>
                <value>少年三国志</value>
            </set>
        </property>

<!--        list注入-->
        <property name="hobbies">
            <list>
                <value>打游戏</value>
                <value>打羽毛球</value>
            </list>
        </property>

<!--        map注入-->
        <property name="grade">
            <map>
                <entry key="高等数学" value="85"/>
                <entry key="大学英语" value="90"/>
                <entry key="java基础" value="98"/>
            </map>
        </property>

<!--        prop注入，类似map-->
        <property name="info">
            <props>
                <prop key="email">administrator@example.org</prop>
                <prop key="phone">13389453895</prop>
                <prop key="QQ-number">456382987</prop>
            </props>
        </property>

    </bean>

</beans>
```





输出预览：

![image-20210730173016148](https://gitee.com/ajream/images/raw/master/img/2021-07-30 17-30-17_image-20210730173016148.png)

